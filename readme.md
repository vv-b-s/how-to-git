# Introduction
Upon joining this group you will be given access to some git repositories.
When you open each one of them, you will be lead to the source code of the `master` branch.
It is important to keep the `master` branch clean with tested working code.

The purpose of this tutorial is to teach you how to work safely with the source code and add only the best of your code to the `master` branch.
By following the steps below you should be able to create your own environment, where you will be able to modify the code however you like, without affecting the `master` branch of the original repository.

# Generating SSH key pair

> _Secure Shell (SSH) is a cryptographic network protocol for operating network services securely over an unsecured network._ - [Wikipedia](https://en.wikipedia.org/wiki/Secure_Shell)

You can connect to your repositories and manage them by using an SSH keypair.
This identifies that the person managing the repository is you.
Usually SSH worked only on Unix-like operating systems like Linux and MacOS, but most recent versions of Windows 10 support SSH natively out of the box.
This section will guide you how to simply generate your ssh key pair.
If you want to research more on this topic visit [this page](https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent).

## Prerequisites

* For Unix-like systems you will use the terminal that came with your OS
* For Windows 10 use PowerShell

## Steps

1) Open the shell for your operating system
2) Go to the home directory of your user

> Note: On Linux for example this usually is `/home/<user>` and on Windows this is `C:\Users\<User>`

3) Check if the folder `.ssh` is created if not, create it with `mkdir .ssh`
4) Create the SSH key pair:

```shell
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```
> Note: Here you will be asked for the save location and passphrases.
Leave those as is by just pressing enter.

5) At the end of the process in your `.ssh` folder two new files will appear:

* `id_rsa` - a no extension file which represents your **private key**.
That said it is important to keep your private key **private** just like you wouldn't share your password!
* `id_rsa.pub` - this is your public key which you can give to anybody (to GitLab in our case).

> Q: How do the keys work?<br/>
A: In a nutshell when you give your public key to some one, it works like a padlock.
Your public key represents an unlocked padlock which everybody can lock or encrypt data with it.
On the other hand your private key represents the key to your padlock, which is owned only by yourself.

6) Go to the settings of your GitLab account and navigate to **SSH Keys** on the sidebar.
7) On the page you will see text field where you can paste the contents of your public key `id_rsa.pub` and a title field.
8) Place the key data inside and click **Add key**.
After that you will be able to operate with your repositories with no problems.


# Contributing to projects

## 1. Forking
The first step is to fork the original project.
This will make a copy of the repository and all its branches into your account as your own repository.
There you are able to modify, play and mess up the code however you please, as long as your `merge requests` are not conflicting with the `master` branch of the original project.

To fork the project press the **Fork** button on the home page of the project
![Fork](res/img/fork.png)

After successful operation a copy of the project will appear in your projects list as your own
![own project](res/img/own-proj.png) 

## 2. Cloning 
Cloning the project requires some speciffic actions you need to take:
1) Clone the master branch of your repository:
```shell
git clone your_repo_url
```

2) After successful cloning you will need to link the original repo to your repo.
This is done to keep your repo up to date with the original repo:
```shell
git remote add upstream original_repo_url
```

> `upstream` is the name given to the `remote` repository.
A `remote` repository is a repository which is not local (not on your computer).
For your forked repository the remote repository is called `origin`.
When performing git commands to `origin` you can ommit the `origin` word in your commands.
The name `upstream` represents the origin of the origin.
When you want to do actions with the `upstream` you must explicitly add `upstream` to your git commands.
You can name the upstream something different as long as you remember the name resembles upstream.

3) Upon setting the `upstream` of the project you will have to update your repositories from time to time.
For most of the actions git itself will navigate you on how to perform certain operations but here are some of the most common update commands you will use:
```shell
#Get the changes from the upstream
git fetch upstream

#Apply the changes to the current branch from upstream/desired-branch
git rebase upstream/master

#Force update the changes on `origin`
git push -f
```
> Keep in mind that changes will affect the current branch you are on.

> IMPORTANT: DO NOT USE `git pull`!
Pulling changes directly from the repositories will implicitly invoke `git merge`.
`merge` is not the same as `rebase`.
`rebase` tries to adjust the code no matter the version differences whilst `merge` will try to append the code.
If the version of your branch is older than the version of the upstream branch `merge` will cause ugly conflict shits on your code.

## 3. Branching
Performing all changes in the `master` branch is ok for minor changes, but when it comes to changes involving 30+ lines of code and many files, it is better to create a new branch.
This will help you to keep the `master` branch clean and safe from unwanted changes.

To create a branch type: 
```shell
git checkout -b branch-name 
```

> `git checkout branch-name` serves to switch from branch to branch. `-b` is used to create a new branch and switch to it.
Any uncommitted changes made prior to branch creation will be transferred to the new branch, leaving the previous branch unchanged since the last commit.
You should also keep in mind that having uncommited changes on current branch will prevent you from switching to other branches unless they are new.

Additional commands to the branching:
```shell
# Add all files to git
git add .

# Commit changes
git commit -m 'Commit message. Give your best description here'

# Push the new branch to `origin`
git push -u origin new-branch-name

# Push changes to current, existing on `origin` branch
git push
```

## 4. Merge requests
When you are done with implementing a feature you would most likely offer a `merge request`.
**Merge request** is a feature of GitLab which offers user friendly GUI to review new changes before adding them to a certain branch.
If there are issues with the code you can easily see and update the code to fix those issues.

Merge requests are done by following the steps:

1) Create the merge request from your repo's home page
![create request](res/img/mrg-req.png)

2) Select the branch you want to merge and where you want to merge it
![select branch](res/img/sel-branch.png)

3) Assign a reviewer
![assign reviewer](res/img/assign.png)

4) If you have many commits choose the `squash` option.
When the merge is approved the update will be one commit instead of n commits.
![squash](res/img/squash.png)

5) When you're done, submit the request.